import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  details: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  discount: number;

  @Column()
  status: 'Active' | 'Inactive';
}
