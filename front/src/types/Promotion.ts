type Status = "Active" | "Inactive"

type Promotion = {
  id: number;
  name: string;
  startDate: Date;
  endDate: Date;
  details: string;
  discount: number;
  status: Status;

};

export type { Promotion};