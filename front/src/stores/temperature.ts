import { ref, computed } from 'vue'

//store
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'

//library
import axios from 'axios'

//service
import http from '@/services/http'
import temparatureService from '@/services/temparature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  async function calConvert() {
    //result.value = convert(celsius.value)
    loadingStore.doLoad()
    try {
      result.value = await temparatureService.convert(celsius.value)
    } catch (e) {
      console.log(e)
    }
    loadingStore.finish()
  }
  return { valid, celsius, result, calConvert }
})
